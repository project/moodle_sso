# Moodle SSO

This project enables shared user sessions between Drupal and the [Moodle](https://moodle.org) learning platform/course management system.

The Drupal 7 version (7.x-1.0) is still available but is not supported. The Drupal 7 and Drupal 9/10 versions of the module are very similar. However, the Drupal 7 version has some dependencies that the Drupal 9/10 version does not have, including:

- Features
- Services
- Strongarm
- Views

## Requirements

The 3.x branch (compatible with Drupal 9/10) has been tested with Moodle 4.1.

Note: These requirements apply to the Drupal 9/10 version (3.x) as this is the only version that's been recently tested.

- Moodle 4.1 or higher
- The [cannod/moodle-drupalservices](https://github.com/cannod/moodle-drupalservices) Moodle plugin

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

SSO can only happen between Drupal and Moodle installations that can share cookies. This means the following configurations will work:

- `moodle.example.com` / `drupal.example.com`
- `www.example.com` (running Drupal) / `moodle.example.com` (running Moodle)
- `www.example.com` (running Moodle) / `drupal.example.com` (running Drupal)
- Sites with subdirectories should also work:
    - `www.example.com` (running Drupal) / `www.example.com/moodle` (running Moodle)
    - `www.example.com` (running Moodle) / `www.example.com/drupal` (running Drupal)

### Drupal 9/10

To configure the cookie domain, set the `parameters / session.storage.options / cookie_domain` value in your site's `services.yml` file to match the value used on your Moodle site. This value **must** match the value for your Moodle site.

If your sites use subdomains, the `cookie_domain` value should be set as follows:
- `cookie_domain: .example.com`

If your sites use subdirectories, the `cookie_domain` value should be set as follows:
- `cookie_domain: www.example.com` 

### Drupal 7

To configure the cookie domain, set the `$cookie_domain` value in your site's `settings.php` file to match the value used on your Moodle site. This value **must** match the value for your Moodle site.

If your sites use subdomains, the `$cookie_domain` value should be set as follows:
- `$cookie_domain = '.example.com';`

If your sites use subdirectories, the `$cookie_domain` value should be set as follows:
- `$cookie_domain = 'www.example.com';`

### Moodle

Installation instructions for the [cannod/moodle-drupalservices](https://github.com/cannod/moodle-drupalservices) Moodle plugin can be found on the plugin's repository page.

## Troubleshooting

If you experience any issues installing or configuring the module, please first check the module's [issue queue](https://www.drupal.org/project/issues/moodle_sso). If no existing issues help you to resolve your issue, please create a new issue and tag it as a support request.